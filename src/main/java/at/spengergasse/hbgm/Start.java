package at.spengergasse.hbgm;

import at.spengergasse.hbgm.gui.MainWindow;
import at.spengergasse.hbgm.gui.Viewer;
import at.spengergasse.hbgm.module_dummies.*;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class Start {

    public static void main(String[] args) throws Exception{


        Builder builder = new Builder();
        builder.setControlPanel(new DummyControlPanel());
        builder.setFileLoader(new DummyFileLoader());
        builder.setImagePanel(new DummyImagePanel());
        builder.setInfoPanel(new DummyInfoPanel());
        builder.setLookupTable(new DummyLookupTable());
        builder.setPatientRepository(new DummyPatientRepository());
        builder.setPixelMapper(new DummyPixelMapper());
        builder.setPatientBrowser(new DummyPatientBrowser());
        builder.configure();



        Viewer viewer = new Viewer(builder);
        viewer.setSize(800,600);
        viewer.setDefaultCloseOperation(EXIT_ON_CLOSE);
        viewer.setVisible(true);

    }
}
