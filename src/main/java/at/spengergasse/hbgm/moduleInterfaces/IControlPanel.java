package at.spengergasse.hbgm.moduleInterfaces;

/**
 * a control panel must provide the possibility to
 * adjust center, width and alpha value.
 * getters are provided to retrieve these values
 */
public interface IControlPanel extends IModuleBase, IModuleUI {
    public int getWindowCenter();
    public int getWindowWidth();
    public int getAlpha();
}
