package at.spengergasse.hbgm.gui;

import at.spengergasse.hbgm.Builder;
import at.spengergasse.hbgm.IO.ImagePanel;
import at.spengergasse.hbgm.entities.Patient;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreeSelectionModel;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class MainWindow extends JFrame {

    //private PatientBrowser patientTree = new PatientBrowser();



    public MainWindow() throws IOException {

        initFrame();
        initMenu();
        //browserContainer.setViewportView(patientTree);

        EntityManager em = Persistence
                .createEntityManagerFactory("viewer")
                .createEntityManager();

        TypedQuery<Patient> query = em
                .createNamedQuery("Patient.findAll", Patient.class);
        List<Patient> patients = query.getResultList();

        for (Patient p : patients) {
        //    patientBrowser.add(p);
        }

       // patientTree.getSelectionModel().addTreeSelectionListener(new Selector());
       // patientTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    }

    private void initMenu(){
        JMenuBar menuBar = new JMenuBar();
        this.setJMenuBar(menuBar);
        JMenu fileMenu = new JMenu("File");
        menuBar.add(fileMenu);
        JMenuItem loadDirectoryMenu = new JMenuItem("load directory");
        fileMenu.add(loadDirectoryMenu);
    }

    private void initFrame() throws IOException {
       /* this.setTitle("ImageViewer");
        this.setLayout(new BorderLayout());
        JSplitPane split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        split.setDividerLocation(150);
        this.add(split,BorderLayout.CENTER);

        JPanel left = new JPanel(new GridLayout(0,1));
        split.add(left, JSplitPane.LEFT);
        left.add(patientBrowser);
        left.add(infoPanel);
        left.add(controlPanel);
        split.add(imagePanel,JSplitPane.RIGHT);

        //patientBrowser.add(patientTree);
        //this.add(status, BorderLayout.SOUTH);

        patientBrowser.setBorder(new TitledBorder("Patients"));
        infoPanel.setBorder(new TitledBorder("Info"));
        controlPanel.setBorder(new TitledBorder("Contrast"));
        imagePanel.setBorder(new TitledBorder("Image"));

        imagePanel.setLayout(new BorderLayout());
        */
        //imageContainer.add(new ImagePanel(new File("images/sT2W-FLAIR - 401/IM-0001-0002.dcm")), BorderLayout.CENTER);
    }

    public static void main(String[] args) throws IOException {


        MainWindow mw = new MainWindow();
        mw.setSize(800,600);
        mw.setDefaultCloseOperation(EXIT_ON_CLOSE);
        mw.setVisible(true);
    }

    private class Selector implements TreeSelectionListener {
        public void valueChanged(TreeSelectionEvent event) {
            Object obj = event.getNewLeadSelectionPath().getLastPathComponent();
            System.out.println(obj + "*****************************" + "\n" + obj.toString().length());
        }
    }

}
