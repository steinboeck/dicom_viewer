package at.spengergasse.hbgm.gui;

import at.spengergasse.hbgm.Builder;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

public class Viewer extends JFrame{

    private JScrollPane controlPanelArea;
    private JPanel imagePanelArea;
    private JScrollPane browserPanelArea;
    private JScrollPane infoPanelArea;

    private Builder builder;

    public Viewer(Builder builder){
        super("DICOM VIEWER");
        this.builder = builder;
        initWindow();
        initMenu();
        addUIModules();
    }

    private void addUIModules(){
        imagePanelArea.add(builder.getImagePanel().uiComponent());
        browserPanelArea.setViewportView(builder.getPatientBrowser().uiComponent());
        infoPanelArea.setViewportView(builder.getInfoPanel().uiComponent());
        controlPanelArea.setViewportView(builder.getControlPanel().uiComponent());


    }

    private void initWindow(){
        this.setLayout(new BorderLayout());

        JSplitPane split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        this.add(split, BorderLayout.CENTER);
        split.setDividerLocation(150);

        JPanel left = new JPanel(new GridLayout(0,1));
        split.add(left, JSplitPane.LEFT);



        browserPanelArea = new JScrollPane();
        browserPanelArea.setBorder(new TitledBorder("Patients"));
        left.add(browserPanelArea);

        infoPanelArea = new JScrollPane();
        infoPanelArea.setBorder(new TitledBorder("Info"));
        left.add(infoPanelArea);

        controlPanelArea = new JScrollPane();
        controlPanelArea.setBorder(new TitledBorder("Controls"));
        left.add(controlPanelArea);
        imagePanelArea = new JPanel();
        imagePanelArea.add(builder.getImagePanel().uiComponent(), BorderLayout.CENTER);
        split.add(imagePanelArea, JSplitPane.RIGHT);




    }

    private void initMenu(){
        JMenuBar menuBar = new JMenuBar();
        this.setJMenuBar(menuBar);
        JMenu fileMenu = new JMenu("File");
        menuBar.add(fileMenu);
        JMenuItem loadDirectoryMenu = new JMenuItem("load directory");
        fileMenu.add(loadDirectoryMenu);
    }
}
