package at.spengergasse.hbgm.moduelImplementaion;

import at.spengergasse.hbgm.Builder;
import at.spengergasse.hbgm.entities.Instance;
import at.spengergasse.hbgm.entities.Patient;
import at.spengergasse.hbgm.entities.Series;
import at.spengergasse.hbgm.entities.Study;
import at.spengergasse.hbgm.moduleInterfaces.IObservable;
import at.spengergasse.hbgm.moduleInterfaces.IObserver;
import at.spengergasse.hbgm.moduleInterfaces.IPatientBrowser;

import javax.swing.*;
import java.util.HashSet;
import java.util.Set;

public class PatientBrowserImplementation implements IPatientBrowser {

    private JTree tree;
    private Set<IObserver> observers = new HashSet<>();

    public PatientBrowserImplementation(){
        tree.addTreeSelectionListener(x -> selectionChanged());
    }

    private void selectionChanged() {

    }

    @Override
    public Patient selectedPatient() {
        return null;
    }

    @Override
    public Study selectedStudy() {
        return null;
    }

    @Override
    public Series selectedSeries() {
        return null;
    }

    @Override
    public Instance selectedInstance() {
        return null;
    }

    @Override
    public void configure(Builder builder) {

    }

    @Override
    public JComponent uiComponent() {
        return null;
    }

    @Override
    public void registerObserver(IObserver o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(IObserver o) {
        observers.remove(o);
    }

    @Override
    public void changed(IObservable o) {

    }
}
