package at.spengergasse.hbgm.module_dummies;

import at.spengergasse.hbgm.Builder;
import at.spengergasse.hbgm.moduleInterfaces.IControlPanel;
import at.spengergasse.hbgm.moduleInterfaces.IObservable;
import at.spengergasse.hbgm.moduleInterfaces.IObserver;

import javax.swing.*;

public class DummyControlPanel implements IControlPanel {
    @Override
    public int getWindowCenter() {
        return 0;
    }

    @Override
    public int getWindowWidth() {
        return 0;
    }

    @Override
    public int getAlpha() {
        return 0;
    }

    @Override
    public JComponent uiComponent() {
        return new JLabel("dummy control panel");
    }

    @Override
    public void configure(Builder builder) {

    }

    @Override
    public void registerObserver(IObserver o) {

    }

    @Override
    public void removeObserver(IObserver o) {

    }

    @Override
    public void changed(IObservable o) {

    }
}
