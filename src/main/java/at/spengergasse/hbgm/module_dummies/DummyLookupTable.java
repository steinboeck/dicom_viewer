package at.spengergasse.hbgm.module_dummies;

import at.spengergasse.hbgm.Builder;
import at.spengergasse.hbgm.moduleInterfaces.ILookupTable;
import at.spengergasse.hbgm.moduleInterfaces.IObservable;
import at.spengergasse.hbgm.moduleInterfaces.IObserver;

public class DummyLookupTable implements ILookupTable {
    @Override
    public int argb(int value) {
        return 0;
    }

    @Override
    public void configure(Builder builder) {

    }

    @Override
    public void registerObserver(IObserver o) {

    }

    @Override
    public void removeObserver(IObserver o) {

    }

    @Override
    public void changed(IObservable o) {

    }
}
