package at.spengergasse.hbgm.module_dummies;

import at.spengergasse.hbgm.Builder;
import at.spengergasse.hbgm.moduleInterfaces.IImagePanel;
import at.spengergasse.hbgm.moduleInterfaces.IObservable;
import at.spengergasse.hbgm.moduleInterfaces.IObserver;

import javax.swing.*;
import java.net.URL;

public class DummyImagePanel implements IImagePanel {
    @Override
    public void configure(Builder builder) {

    }

    @Override
    public JComponent uiComponent() {
        URL url = getClass().getResource("/homer.jpg");
        //return new JLabel(new ImageIcon(url));
        return new JLabel("dummy image panel");
    }

    @Override
    public void registerObserver(IObserver o) {

    }

    @Override
    public void removeObserver(IObserver o) {

    }

    @Override
    public void changed(IObservable o) {

    }
}
