package at.spengergasse.hbgm.module_dummies;

import at.spengergasse.hbgm.Builder;
import at.spengergasse.hbgm.moduleInterfaces.IFileLoader;
import at.spengergasse.hbgm.moduleInterfaces.IObservable;
import at.spengergasse.hbgm.moduleInterfaces.IObserver;


import java.io.File;

public class DummyFileLoader implements IFileLoader {
    @Override
    public void loadFolder(File folder) throws Exception {

    }

    @Override
    public void configure(Builder builder) {

    }

    @Override
    public void registerObserver(IObserver o) {

    }

    @Override
    public void removeObserver(IObserver o) {

    }

    @Override
    public void changed(IObservable o) {

    }
}
