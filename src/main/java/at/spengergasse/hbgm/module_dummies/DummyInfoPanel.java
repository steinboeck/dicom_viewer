package at.spengergasse.hbgm.module_dummies;

import at.spengergasse.hbgm.Builder;
import at.spengergasse.hbgm.moduleInterfaces.IInfoPanel;
import at.spengergasse.hbgm.moduleInterfaces.IObservable;
import at.spengergasse.hbgm.moduleInterfaces.IObserver;

import javax.swing.*;

public class DummyInfoPanel implements IInfoPanel {
    @Override
    public void configure(Builder builder) {

    }

    @Override
    public JComponent uiComponent() {
        return new JLabel("dummy info panel");
    }

    @Override
    public void registerObserver(IObserver o) {

    }

    @Override
    public void removeObserver(IObserver o) {

    }

    @Override
    public void changed(IObservable o) {

    }
}
